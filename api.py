from db import restaurant_database
from flask import Flask, request
from flask_restful import Resource, Api
import json

client = restaurant_database()
app = Flask(__name__)
api = Api(app)


class CreateDish(Resource):
    def post(self):
        dish_json = request.get_json()
        dish_string = json.dumps(dish_json)
        dish_object = json.loads(dish_string)
        client.insert_one(dish_object)

        return {'you sent': dish_json}, 201


class GetDishes(Resource):
    def get(self):
        count = client.count_documents({})
        if count < 1:
            return {"dishes": "There are no dishes in the database."}, 404

        dishes_db = client.find({})
        dishes = []

        for dish in dishes_db:
            dishes.append(dish)

        return {"dishes": dishes}, 200

class GetDish(Resource):
    def get(self, dish_id):
        dish = client.find_one({"_id": dish_id})
        if not dish:
            return {"dish": "No dish exists with this ID."}
        
        return {"dish": dish}, 200



class UpdateDish(Resource):
    def put(self, dish_id, food):
        client.update_one({"id": dish_id}, {"$set": {"dish": food}})
        return {}, 201


class DeleteDish(Resource):
    def delete(self, dish_id):
        dish = {"id": dish_id}
        client.delete_one(dish)
        return '', 204


api.add_resource(CreateDish, '/')
api.add_resource(GetDish, '/dishes/<int:dish_id>')
api.add_resource(GetDishes, '/dishes')
api.add_resource(UpdateDish, '/update-dish/<int:dish_id>/<string:food>')
api.add_resource(DeleteDish, '/delete-dish/<int:dish_id>')

