from pymongo import MongoClient
from constants import MONGO_DB_URI

def restaurant_database():
    client = MongoClient(MONGO_DB_URI, ssl=True, ssl_cert_reqs='CERT_NONE')
    db = client["restaurant"]
    coll = db["dishes"]
    return coll
